package cz.dwh.DwhExample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class MujRestovejKontroler {
    
    @Autowired
    @Qualifier("mojeService")
    private MojeService mojeService;
    
    @GetMapping("get")
    public void mujGet() {
        mojeService.saveFactTable();
    }
    
    @GetMapping("get2")
    public void mujGetDva() {
        System.out.println("get 2");
    }
}
