package cz.dwh.DwhExample;

import cz.dwh.DwhExample.repository.FactTableRepository;
import org.springframework.stereotype.Service;

@Service
public class MojeService {

    private FactTableRepository factTableRepository;
    
    public MojeService(final FactTableRepository factTableRepository) {
        this.factTableRepository = factTableRepository;
    }
    
    public void saveFactTable() {
        FactTable ft = new FactTable("muj test");
        this.factTableRepository.save(ft);
    }

}
