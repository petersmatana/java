package cz.dwh.DwhExample;

import org.springframework.stereotype.Component;

@Component
public class MojeKomponenta {
    public MojeKomponenta() {
        metoda();
    }
    
    public void metoda() {
        System.out.println("toto je moje metoda v komponente");
    }
}
