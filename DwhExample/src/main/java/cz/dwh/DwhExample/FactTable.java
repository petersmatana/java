package cz.dwh.DwhExample;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "fact")
public class FactTable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String test;
    
    public FactTable() {
    }
    
    public FactTable(String test) {
        this.test = test;
    }
    
    @Override
    public String toString() {
        return "FactTable{" +
        "id=" + id +
        ", test='" + test + '\'' +
        '}';
    }
}
