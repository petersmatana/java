package cz.dwh.DwhExample.repository;

import cz.dwh.DwhExample.FactTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FactTableRepository extends JpaRepository<FactTable, Long> {
}
