package cz.dwh.DwhExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DwhExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(DwhExampleApplication.class, args);
	}

}
