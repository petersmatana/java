package cz.pkg.better.controllers;

import org.springframework.stereotype.Controller;

@Controller
public class MyController {
    public String hello() {
        String data = "Hello from MyController";
        System.out.println(data);
        return data;
    }
}
