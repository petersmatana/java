package cz.pkg.better.controllers;

import cz.pkg.better.services.GreetingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ConstructorInjectedController {

    public GreetingServiceImpl greetingService;

    // zde dochazi k automatickemu zjistovani ktera
    // komponenta se sem nejvic hodi ale stejne to muzu
    // udelat
    @Autowired
    public ConstructorInjectedController(GreetingServiceImpl greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHello() {
        return this.greetingService.seyGreetings();
    }
}
