package cz.pkg;

import cz.pkg.bad.controllers.MyController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);

		MyController controller = (MyController) context.getBean("myController");
		controller.hello();

		// takto muzu pokracovat ale zadnou tridu nemam
		// oanotovanou takze ji nemuzu hledat jako bean
		// System.out.println(context.getBean(ConstructorInjectedController.class).greetingService.seyGreetings());
	}

}
