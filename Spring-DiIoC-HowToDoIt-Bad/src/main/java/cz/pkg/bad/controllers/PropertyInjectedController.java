package cz.pkg.bad.controllers;

import cz.pkg.bad.services.GreetingServiceImpl;

public class PropertyInjectedController {

    // chyba, toto by melo byt private a zaroven by
    // zde mel byt interface
    public GreetingServiceImpl greetingService;

    public String sayHello() {
        return greetingService.seyGreetings();
    }
}
