package cz.pkg.bad.controllers;


import cz.pkg.bad.services.GreetingServiceImpl;

public class SetterInjectedController {
    public GreetingServiceImpl greetingService;

    public String sayHello() {
        return greetingService.seyGreetings();
    }

    public void setGreetingService(GreetingServiceImpl greetingService) {
        this.greetingService = greetingService;
    }
}
