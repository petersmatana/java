package cz.pkg.bad.controllers;

import cz.pkg.bad.services.GreetingServiceImpl;

public class ConstructorInjectedController {
    public GreetingServiceImpl greetingService;

    public ConstructorInjectedController(GreetingServiceImpl greetingService) {
        this.greetingService = greetingService;
    }

    String sayHello() {
        return this.greetingService.seyGreetings();
    }
}
