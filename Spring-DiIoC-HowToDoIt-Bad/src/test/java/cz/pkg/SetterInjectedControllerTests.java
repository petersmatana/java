package cz.pkg;

import cz.pkg.bad.controllers.SetterInjectedController;
import cz.pkg.bad.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SetterInjectedControllerTests {

    private SetterInjectedController setterInjectedController;

    @Before
    public void before() {
        this.setterInjectedController = new SetterInjectedController();
        this.setterInjectedController.setGreetingService(new GreetingServiceImpl());
    }

    @Test
    public void greetingTest() {
        Assert.assertEquals(GreetingServiceImpl.HELLO,
                this.setterInjectedController.sayHello());
    }

    @Test
    public void newLineTest() {
        String test = this.setterInjectedController.sayHello() + "\n";
        Assert.assertNotEquals(GreetingServiceImpl.HELLO, test);
    }

}
