package cz.pkg;

import cz.pkg.bad.controllers.ConstructorInjectedController;
import cz.pkg.bad.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ConstructorInjectedControllerTests {

    private ConstructorInjectedController constructorInjectedController;

    @Before
    public void before() {
        this.constructorInjectedController = new ConstructorInjectedController(new GreetingServiceImpl());
    }

    @Test
    public void greetingTest() {
        Assert.assertEquals(GreetingServiceImpl.HELLO,
                this.constructorInjectedController.greetingService.seyGreetings());
    }

    @Test
    public void newLineTest() {
        String test = this.constructorInjectedController.greetingService.seyGreetings() + "\n";
        Assert.assertNotEquals(GreetingServiceImpl.HELLO,test);
    }
}
