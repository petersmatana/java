package cz.pkg;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringDiIocJaktonedelatApplicationTests {

	@Test
	void contextLoads() {
		Assertions.assertEquals(1,1);
	}

	@Test
	void fail() {
		Assertions.assertEquals(1, 2);
	}

}
