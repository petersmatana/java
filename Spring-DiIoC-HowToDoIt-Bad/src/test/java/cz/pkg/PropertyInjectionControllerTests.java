package cz.pkg;

import cz.pkg.bad.controllers.PropertyInjectedController;
import cz.pkg.bad.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PropertyInjectionControllerTests {

    private PropertyInjectedController propertyInjectedController;

    @Before
    public void before() {
        this.propertyInjectedController = new PropertyInjectedController();
        this.propertyInjectedController.greetingService = new GreetingServiceImpl();
    }

    @Test
    public void greetingTest() {
        Assert.assertEquals(GreetingServiceImpl.HELLO,
                this.propertyInjectedController.sayHello());
    }

    @Test
    public void newLineTest() {
        String test = this.propertyInjectedController.sayHello() + "\n";
        Assert.assertNotEquals(GreetingServiceImpl.HELLO, test);
    }

}
