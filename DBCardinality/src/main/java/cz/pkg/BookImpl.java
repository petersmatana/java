package cz.pkg;

import cz.pkg.base.Book;
import org.springframework.stereotype.Service;

@Service
public class BookImpl implements Book {

    private String title;

    public BookImpl() {
    }

    public void setTitle(String t) {
        title = t;
    }
}
