package cz.pkg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbCardinalityApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbCardinalityApplication.class, args);
	}

}
