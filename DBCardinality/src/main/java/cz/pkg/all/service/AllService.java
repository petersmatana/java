package cz.pkg.all.service;

import cz.pkg.all.entity.AllAuthor;
import cz.pkg.all.entity.AllBook;
import cz.pkg.all.repository.AllAuthorRepository;
import cz.pkg.all.repository.AllBookRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AllService {
    private AllAuthorRepository allAuthorRepository;

    private AllBookRepository allBookRepository;

    public AllService(
            final AllAuthorRepository allAuthorRepository,
            final AllBookRepository allBookRepository
    ) {
        this.allAuthorRepository = allAuthorRepository;
        this.allBookRepository = allBookRepository;

        startup();
        deleteAuthor();
    }

    public void startup() {
        AllAuthor author = new AllAuthor("a1", "cz");
        this.allAuthorRepository.save(author);

        AllBook b1 = new AllBook("b1", "isbn1");
        this.allBookRepository.save(b1);
        AllBook b2 = new AllBook("b2", "isbn2");
        this.allBookRepository.save(b2);
        AllBook b3 = new AllBook("b3", "isbn3");
        this.allBookRepository.save(b3);

        Optional<AllAuthor> findAuthors = this.allAuthorRepository.findById(Long.valueOf(1));
        if (findAuthors.isPresent()) {
            System.out.println(findAuthors.toString());
        }

        Iterable<AllBook> findBooks = this.allBookRepository.findAll();
        for (AllBook book : findBooks) {
            System.out.println(book.toString());
        }
    }

    public void deleteAuthor() {
        Optional<AllAuthor> findAuthors = this.allAuthorRepository.findById(Long.valueOf(1));

        if (findAuthors.isPresent()) {
            this.allAuthorRepository.delete(findAuthors.get());
        }

        findAuthors = this.allAuthorRepository.findById(Long.valueOf(1));
        if (findAuthors.isPresent()) {
            System.out.println("Nekde se stala chyba.");
        } else {
            System.out.println("Author je spravne smazan.");
        }
    }
}
