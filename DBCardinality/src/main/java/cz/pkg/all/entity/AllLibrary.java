package cz.pkg.all.entity;

import cz.pkg.base.Library;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
public class AllLibrary implements Library {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany
    private Set<AllBook> books;

    private String name;

    private String city;

    public AllLibrary() {
    }

    public AllLibrary(final String name, final String city) {
        this.name = name;
        this.city = city;
    }

    @Override
    public String toString() {
        return "AllLibrary{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
