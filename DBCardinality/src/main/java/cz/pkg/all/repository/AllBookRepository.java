package cz.pkg.all.repository;

import cz.pkg.all.entity.AllBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AllBookRepository extends JpaRepository<AllBook, Long> {
}
