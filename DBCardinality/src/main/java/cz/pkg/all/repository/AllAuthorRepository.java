package cz.pkg.all.repository;

import cz.pkg.all.entity.AllAuthor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AllAuthorRepository extends JpaRepository<AllAuthor, Long> {
}
