package cz.pkg.all;

import cz.pkg.all.entity.AllAuthor;
import cz.pkg.all.entity.AllBook;
import cz.pkg.all.repository.AllAuthorRepository;
import cz.pkg.all.repository.AllBookRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class AllTest {
    private final Logger logger = LogManager.getLogger(AllTest.class);

    @Autowired
    private AllAuthorRepository allAuthorRepository;

    @Autowired
    private AllBookRepository allBookRepository;

//    private AllAuthor author = new AllAuthor("a1", "cz");
//
//    private AllBook b1 = new AllBook("b1", "isbn1");
//    private AllBook b2 = new AllBook("b2", "isbn2");
//    private AllBook b3 = new AllBook("b3", "isbn3");

    private AllAuthor author;

    private AllBook b1;
    private AllBook b2;
    private AllBook b3;

    public void before() {
        logger.info("before test");
        author = new AllAuthor("a1", "cz");
        allAuthorRepository.saveAndFlush(author);

        b1 = new AllBook("b1", "isbn1");
        allBookRepository.save(b1);

        b2 = new AllBook("b2", "isbn2");
        allBookRepository.save(b2);

        b3 = new AllBook("b3", "isbn3");
        allBookRepository.save(b3);

        List<AllBook> booksList = new LinkedList<>();
        booksList.add(b1);
        booksList.add(b2);
        booksList.add(b3);

        author.setBooks(booksList);
        allAuthorRepository.saveAndFlush(author);
    }

    public void after() {
        logger.info("after test");
        allAuthorRepository.deleteAll();
        allBookRepository.deleteAll();
    }

    @Test
    public void testAuthorBook() throws Exception {
        before();
        logger.info("asd");

        List<AllAuthor> findAuthors = allAuthorRepository.findAll();
        Assertions.assertEquals(findAuthors.get(0), author);

        allAuthorRepository.delete(findAuthors.get(0));

        findAuthors = allAuthorRepository.findAll();
        Assertions.assertEquals(findAuthors, Collections.emptyList());

        List<AllBook> findBooks = allBookRepository.findAll();
        for (AllBook book : findBooks) {
            if (!(book.equals(b1) || book.equals(b2) || book.equals(b3))) {
                throw new Exception("je to prasarna nicmene zde test NEPROSEL");
            }
        }

        after();
    }
}
