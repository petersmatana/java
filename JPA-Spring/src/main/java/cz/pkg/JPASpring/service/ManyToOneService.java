package cz.pkg.JPASpring.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import cz.pkg.JPASpring.entities.ManyToOne.Department;
import cz.pkg.JPASpring.entities.ManyToOne.Employee;
import cz.pkg.JPASpring.repositories.ManyToOne.DepartmentRepository;
import cz.pkg.JPASpring.repositories.ManyToOne.EmployeeRepository;

@Component
public class ManyToOneService {

	@Autowired
	@Qualifier("departmentRepository")
	private DepartmentRepository departmentRepository;
	
	@Autowired
	@Qualifier("employeeRepository")
	private EmployeeRepository employeeRepository;
	
	public void setEmployeeDepartment(Employee employee, Department department) {
		Objects.requireNonNull(employee);
		Objects.requireNonNull(department);
		
		employee.setDepartment(department);
		
		// there should be some transaction
		
		this.employeeRepository.save(employee);
	}
	
	public void saveEmployee(Employee employee) {
		Objects.requireNonNull(employee);
		
		this.employeeRepository.save(employee);
	}
	
	public void saveDepartment(Department department) {
		Objects.requireNonNull(department);
		
		this.departmentRepository.save(department);
	}
	
	public void getEmployeesFromSDDepartment() {
//		Department department = this.departmentRepository.findByName("Software Development");
//		
//		List<Employee> employees = this.departmentRepository.findAllEmployeesByDepartment(department);
//		for (Employee employee : employees) {
//			System.out.println(employee);
//		}
	}
}
