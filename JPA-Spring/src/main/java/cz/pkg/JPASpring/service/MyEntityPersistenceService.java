package cz.pkg.JPASpring.service;

import cz.pkg.JPASpring.entities.MyEntity;
import cz.pkg.JPASpring.repositories.MyEntityPagingAndSortingRepository;
import cz.pkg.JPASpring.repositories.MyEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyEntityPersistenceService {
  @Autowired
  @Qualifier("myEntityRepository")
  private MyEntityRepository repository;

  @Autowired
  @Qualifier("myEntityPagingAndSortingRepository")
  private MyEntityPagingAndSortingRepository pagingAndSortingRepository;

  public void add(MyEntity myEntity) {
    this.repository.save(myEntity);
  }

  public List<MyEntity> findAllByNumber(int page, int size, int number) {
    Pageable pageable = PageRequest.of(page, size);

    return this.pagingAndSortingRepository.findAllByNumber(number, pageable);
  }

  public void findAll() {
    for (MyEntity e : this.repository.findAll()) {
      System.out.println(e);
    }
  }
}
