package cz.pkg.JPASpring.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "myentity", schema = "public")
public class MyEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "text")
  private String text;

  @Column(name = "number")
  private int number;

  public MyEntity() {
  }

  public MyEntity(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  @Override
  public String toString() {
    return "MyEntity{" +
        "id=" + id +
        ", text='" + text + '\'' +
        ", number=" + number +
        '}';
  }
}
