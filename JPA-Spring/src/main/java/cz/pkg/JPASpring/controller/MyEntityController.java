package cz.pkg.JPASpring.controller;

import cz.pkg.JPASpring.entities.MyEntity;
import cz.pkg.JPASpring.service.MyEntityPersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/go")
public class MyEntityController {
  @Autowired
  private MyEntityPersistenceService myEntity;

  @GetMapping("/")
  public void go() {
    setUpData();
    page();
    all();
  }

  private void setUpData() {
    MyEntity e1 = new MyEntity();
    e1.setText("ahoj");
    e1.setNumber(13);

    this.myEntity.add(e1);

    MyEntity e2 = new MyEntity();
    e2.setText("toto je text");
    e2.setNumber(1);

    this.myEntity.add(e2);

    MyEntity e3 = new MyEntity();
    e3.setText("jiny text");
    e3.setNumber(2);

    this.myEntity.add(e3);

    MyEntity e4 = new MyEntity();
    e4.setText("asd www");
    e4.setNumber(42);

    this.myEntity.add(e4);
  }

  private void page() {
    System.out.println("Page");
    System.out.println(this.myEntity.findAllByNumber(1, 10, 13) + "\n");
  }

  private void all() {
    System.out.println("All");
    this.myEntity.findAll();
    System.out.println();
  }
}
