package cz.pkg.JPASpring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.pkg.JPASpring.entities.ManyToOne.Department;
import cz.pkg.JPASpring.entities.ManyToOne.Employee;
import cz.pkg.JPASpring.service.ManyToOneService;

@RestController
@RequestMapping("/manytoone")
public class ManyToOneController {

	@Autowired
	private ManyToOneService manyToOneService;
	
	@GetMapping("/go")
	public void go() {
		Department SD = new Department();
		SD.setName("Software Development");
		
		this.manyToOneService.saveDepartment(SD);
		
		Employee employee1 = new Employee();
		employee1.setName("Peter");
		
		this.manyToOneService.saveEmployee(employee1);
		this.manyToOneService.setEmployeeDepartment(employee1, SD);
		
		Employee employee2 = new Employee();
		employee2.setName("Pepa");
		
		this.manyToOneService.saveEmployee(employee2);
		this.manyToOneService.setEmployeeDepartment(employee2, SD);
		
		Employee employee3 = new Employee();
		employee3.setName("Franta");
		
		this.manyToOneService.saveEmployee(employee3);
		this.manyToOneService.setEmployeeDepartment(employee3, SD);
		
		Department NA = new Department();
		NA.setName("Network Administration");
		
		this.manyToOneService.saveDepartment(NA);
		
		Employee employee4 = new Employee();
		employee4.setName("Tom");
		
		this.manyToOneService.saveEmployee(employee4);
		this.manyToOneService.setEmployeeDepartment(employee4, NA);
	}
	
	@GetMapping("/sd")
	public void getSAEmployees() {
		this.manyToOneService.getEmployeesFromSDDepartment();
	}
}
