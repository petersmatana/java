package cz.pkg.JPASpring.repositories;

import cz.pkg.JPASpring.entities.MyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MyEntityPagingAndSortingRepository extends PagingAndSortingRepository<MyEntity, Long> {
  List<MyEntity> findAllByNumber(int number, Pageable pageable);
}
