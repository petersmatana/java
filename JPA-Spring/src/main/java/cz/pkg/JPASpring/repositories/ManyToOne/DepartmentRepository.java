package cz.pkg.JPASpring.repositories.ManyToOne;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cz.pkg.JPASpring.entities.ManyToOne.Department;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long>{
	public Department findByName(String name);
}
