package cz.pkg.JPASpring.repositories.ManyToOne;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cz.pkg.JPASpring.entities.ManyToOne.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
