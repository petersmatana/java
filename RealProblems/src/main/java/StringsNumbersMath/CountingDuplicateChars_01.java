package StringsNumbersMath;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

class CountingDuplicateChars_01 {
    static String input = "asdASD123@!#asdASD123@!#asdeeeee";

    public static void main(String[] args) {
//        first();
//        second();
//        third();

        printAllUnicodeChars();
    }

    public static void printAllUnicodeChars() {
        for (int i = 0; i < 65535; i++) {
            System.out.println((char) i);
        }
    }

    public static void third() {
        Map<Character, Long> result = input
                .chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));

        printOutput(result);
    }

    public static void second() {
        Map<Character, Integer> result = new HashMap<>();
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);

            result.compute(ch, (k, v) -> {
                if (v == null) {
                    v = 1;
                } else {
                    v += 1;
                }

                return v;
            });
        }

        printOutput(result);
    }

    public static void first() {
        Map<Character, Integer> result = new HashMap<>();
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);

            if (result.containsKey(ch)) {
                result.put(ch, result.get(ch) + 1);
            } else {
                result.put(ch, 1);
            }
        }

        printOutput(result);
    }

    private static <T, U> void printOutput(Map<T, U> input) {
        for (Map.Entry<T, U> element : input.entrySet()) {
            System.out.println("key: " + element.getKey() + " value: " + element.getValue());
        }
    }
}