package Comparator;

import Function.MyFunction;

import java.util.Objects;

@FunctionalInterface
public interface MyComparator<T> {
  int compare(T t1, T t2);

  static <T> MyComparator<T> stringCompare(MyFunction<T, String> keyExtractor) {
    Objects.requireNonNull(keyExtractor);
    return (T t1, T t2) -> {
      String string1 = keyExtractor.apply(t1);
      String string2 = keyExtractor.apply(t2);

      return string1.compareTo(string2);
    };
  }

//  static <T> MyComparator<T> inverseCompare(MyFunction<T, String> keyExtractor) {
//    Objects.requireNonNull(keyExtractor);
//    return (T t1, T t2) -> {
//      String string1 = keyExtractor.apply(t1);
//      String string2 = keyExtractor.apply(t2);
//
//      return -string1.compareTo(string2);
//    };
//  }

  default MyComparator<T> inverseCompare() {
    return (T t1, T t2) -> -this.compare(t1, t2);
  }
}
