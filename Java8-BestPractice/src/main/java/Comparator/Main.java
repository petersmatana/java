package Comparator;

import Function.MyFunction;

public class Main {
  public static void main(String[] args) {
//    basic();
//    comparator_1();
//    comparator_2();
    comparator_3();
  }

  /*zde pouzivam fce z interface MyComparator
  * */
  private static void comparator_3() {
    Person p1 = new Person("Peter", 30);
    Person p2 = new Person("Tomas", 13);
    Person p3 = new Person("Honza", 22);

    MyFunction<Person, String> getName = (Person person) -> {
      return person.getName();
    };

    MyComparator<Person> compare = MyComparator.stringCompare(getName);
    System.out.println("string comparator = " + compare.compare(p1, p2));

//    MyComparator<Person> inverse = compare.compare(p1, p2);
//    System.out.println("inverse compare = " + );
  }

  private static void comparator_2() {
    Person p1 = new Person("Peter", 30);
    Person p2 = new Person("Tomas", 13);
    Person p3 = new Person("Honza", 22);

    MyFunction<Person, String> getName = (Person person) -> {
      return person.getName();
    };

    MyComparator<Person> name = compare_2(getName);

    int nameTest = name.compare(p1, p3);
    System.out.println("name test = " + nameTest);
  }

  /*toto je faktory metoda ktera protoze vraci MyComparator
  tuhle metodu muzu zobecnit do interface coz je treti krok
  v tutorialu...
  * */
  private static MyComparator<Person> compare_2(MyFunction<Person, String> getName) {
    return (Person person1, Person person2) -> {
      String name1 = getName.apply(person1);
      String name2 = getName.apply(person2);

      return name1.compareTo(name2);
    };
  };

  /*prvni pokus o implementaci komparatoru,
  vylepseni prichazi v metode comparator_2()
  * */
  private static void comparator_1() {
    Person p1 = new Person("Peter", 30);
    Person p2 = new Person("Tomas", 13);
    Person p3 = new Person("Honza", 22);

    MyComparator<Person> name = (Person person1, Person person2) -> {
      String name1 = person1.getName();
      String name2 = person2.getName();

      return name1.compareTo(name2);
    };

    int nameTest = name.compare(p1, p3);
    System.out.println("name test = " + nameTest);
  }

  private static void basic() {
    Person p1 = new Person("Peter", 30);
    Person p2 = new Person("Tomas", 13);
    Person p3 = new Person("Honza", 22);

    MyComparator<Person> age = (Person person1, Person person2) -> {
      if (person1.getAge() == person2.getAge()) {
        return 0;
      }

      if (person1.getAge() > person2.getAge()) {
        return 1;
      } else {
        return -1;
      }
    };

    int ageTest = age.compare(p1, p2);
    System.out.println("age test = " + ageTest);
  }
}
