package Function.practical_using;

import java.util.function.Function;

public class Main {
  public static void main(String[] args) {
    compose();
    andThen();
  }

  /*
  * */
  private static void compose() {

  }

  /*funkce andThen pracuje opacne jako compose.
  funkce andThen vraci slozenou funkci, ktera
  nejprve pozuije tuto funkci na svuj vstup
  a potom na vysledek aplikuje funkci after
  * */
  private static void andThen() {
    Integer number = 0;

    Function<Integer, String> vysledekInt = (Integer cislo) -> String.valueOf(cislo);
    Function<Integer, Integer> prictiJedna = (Integer cislo) -> cislo + 1;

    System.out.println(prictiJedna.apply(10));
    System.out.println(prictiJedna.andThen(vysledekInt).apply(10));

    Function<Integer, Integer> test = prictiJedna.andThen(prictiJedna);
    test.andThen(prictiJedna);
    System.out.println(test.apply(0));
  }
}
