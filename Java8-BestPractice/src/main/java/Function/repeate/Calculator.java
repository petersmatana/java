package Function.repeate;

public class Calculator {
  private int value;

  public Calculator(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    String result = "Calculator{" +
        "value=" + value +
        '}';
    System.out.println(result);
    return result;
  }
}
