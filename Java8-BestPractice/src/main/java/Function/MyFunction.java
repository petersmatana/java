package Function;

import java.util.Objects;

@FunctionalInterface
/*
T = prijima
R = vraci
* */
public interface MyFunction<T, R> {
  R apply(T t);

  default <V> MyFunction<T, V> andThen(MyFunction<R, V> other) {
    Objects.requireNonNull(other);

    return (T t) -> {
      // na prvek ktery prijde aplikuju apply
      R result = this.apply(t);

      // vracim other.apply ktery prijima result
      return other.apply(result);
    };
  }

  default <V> MyFunction<V, R> compose(MyFunction<V, T> other) {
    return (V v) -> {
      T t = other.apply(v);
      return this.apply(t);
    };
  }
}
