package Function;

public class Meteo {
  private enum types {CELSIUS, FAHRENHEIT, KELVIN};
  private double temperature;

  public Meteo(double temperature) {
    this.temperature = temperature;
  }

  public double getTemperature() {
    return temperature;
  }

  public void setTemperature(double temperature) {
    this.temperature = temperature;
  }

  @Override
  public String toString() {
    String result = "Meteo{" +
        "temperature=" + temperature +
        '}';
    System.out.println(result);
    return result;
  }
}
