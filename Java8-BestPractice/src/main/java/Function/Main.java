package Function;

import java.util.function.Predicate;

public class Main {
  public static void main(String[] args) {
//    basic();
    andThen();
  }

  private static void andThen() {
    Meteo meteo = new Meteo(10);

    MyFunction<Meteo, Double> c = (Meteo m) -> {
      return m.getTemperature();
    };

    // fahrenheit to celsius
    MyFunction<Double, Double> f2c = (Double fahrenheitTemp) -> {
      return (fahrenheitTemp - 32.0) / 1.8;
    };

    // celsius to fahrenheit
    MyFunction<Double, Double> c2f = (Double celsiusTemp) -> {
      return (celsiusTemp * 1.8) + 32.0;
    };

    System.out.println(c.andThen(f2c).apply(meteo));

    System.out.println(c2f.apply(meteo.getTemperature()));
    System.out.println(f2c.apply(meteo.getTemperature()));

    System.out.println(c2f.compose(c).apply(meteo));

//    Predicate
  }

  private static void basic() {
    Meteo meteo = new Meteo(10);

    MyFunction<Meteo, Double> c = (Meteo m) -> {
      return m.getTemperature();
    };

    System.out.println(c.apply(meteo));
  }
}
