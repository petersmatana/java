package Consumer;

public class Main {
  public static void main(String[] args) {
//    createSentenseWithNull();
    chain();
  }

  private static void chain() {
    MyConsumer<String> consumer = (String s) -> {};

    consumer
        .nextDefaultMethod(consumer)
        .andThen(consumer)
        .accept("asd");
  }

  /*Je blbe kdyz andThen prijima null. to musim osetrit
  v interface MyConsumer.
  * */
  private static void createSentenseWithNull() {
    MyConsumer<String> str0 = (String s) -> {
      System.out.print("1 " + s);
    };

    MyConsumer<String> str1 = (String s) -> {
      System.out.print(" 2 " + s + s);
    };

    MyConsumer<String> test = str0
        .andThen(null);
    test.accept("ahoj");
  }

  /*Takto zretezim vypis.
  * */
  private static void then() {
    MyConsumer<String> str0 = x -> {
      System.out.println("str0 = " + x);
    };

    MyConsumer<String> str1 = x -> {
      System.out.println("str1 = " + x);
    };

    MyConsumer<String> str2 = x -> {
      System.out.println("str2 = " + x);
    };

    MyConsumer<String> test = str0
        .andThen(str1)
        .andThen(str2);
    test.accept("hello");
  }

  private static void basic() {
    System.out.println("hello");

    MyConsumer<Integer> digitConsumer = x -> {
      System.out.println("digit consumer = " + x);
    };

    digitConsumer.accept(13);

    MyConsumer<String> stringConsumer = x -> {
      System.out.println("string consumer = " + x);
    };

    stringConsumer.accept("hello");
  }

}
