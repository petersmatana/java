package Consumer;

import java.util.Objects;

@FunctionalInterface
public interface MyConsumer<T> {
  void accept(T t);

  /* osetreni null pointer exception se da vyresit lepe
  * */
//  default MyConsumer<T> andThen(MyConsumer<T> other) throws NullPointerException {
//    return (T t) -> {
//      if (t != null) {
//        this.accept(t);
//        other.accept(t);
//      } else {
//        throw new NullPointerException("Can't be empty.");
//      }
//    };
//  }

  default MyConsumer<T> nextDefaultMethod(MyConsumer<T> other) {
    Objects.requireNonNull(other);
    System.out.println("delam neco dalsiho" + other.toString());
    return (T t) -> {
      other.accept(t);
    };
  }

  static void asd() {

  }

  default MyConsumer<T> andThen(MyConsumer<T> other) throws NullPointerException {
    Objects.requireNonNull(other);
    return (T t) -> {
      this.accept(t);
      other.accept(t);
    };
  }
}
