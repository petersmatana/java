package Consumer.practical_using;

import java.util.function.Consumer;

public class Main {
  public static void main(String[] args) {
    stringConsumer();
  }

  private static void stringConsumer() {
    Consumer<String> textLength = (String string) -> {
      System.out.println("delka textu = " + string.length());
    };

    Consumer<String> addPrefix = (String string) -> {
      System.out.println("prefix-" + string);
    };

    Consumer<String> addPostfix = (String string) -> {
      System.out.println(string + "-postfix");
    };

    Consumer<String> print = (String string) -> {
      System.out.println(string);
    };

    String input = "vstupni text";
    Consumer<String> result = textLength
        .andThen(addPostfix)
        .andThen(addPrefix)
        .andThen(print);
    result.accept(input);
  }
}
