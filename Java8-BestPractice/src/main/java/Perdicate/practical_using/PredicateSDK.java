package Perdicate.practical_using;

import com.sun.tools.classfile.ConstantPool;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateSDK {
    public static void main(String[] args) {
        begin();
    }

    public static void begin() {
        Predicate<String> startPredicate = x -> {
            return true;
        };

        Predicate<String> testPouzeZnaky = x -> {
            return "^[@a-zA-Z0-9 ]+$".matches(x);
        };
        
        BiPredicate<Integer, Integer> mensiNez = (Integer input, Integer test) -> {
            return input > test;
        };
    
        BiPredicate<String, Integer> testNaMaximalniDelku = (String input, Integer test) -> {
            return mensiNez.test(input.length(), test);
        };
        
        BiPredicate<String, Integer> testNaMinimalniDelku = (String input, Integer test) -> {
            return mensiNez.test(input.length(), test);
        };
        
        BiPredicate<Character, Character> porovnejZnaky = (Character input, Character test) -> {
            return input.equals(test);
        };
        
        BiPredicate<String, Character> testNaPrvniZnak = (String input, Character test)  -> {
            return porovnejZnaky.test(input.charAt(0), test);
        };
        
        Predicate<String> zabalenaLogika = (String input) -> {
            return startPredicate
                .and(x -> testNaPrvniZnak.test(x, 'm'))
                .and(x -> testNaMinimalniDelku.test(x, 5))
//                .and(x -> testPouzeZnaky.test(input))
                .test(input);
        };
        
        Predicate<String> kratkyTweet = (String input) -> {
            return startPredicate
                .and(x -> testNaMaximalniDelku.test(x, 13))
//                .and(x -> testPouzeZnaky.test(input))
                .test(input);
        };
        
        if (kratkyTweet.test("@musi to byt kratky")) {
            System.out.println("ok");
        } else {
            System.out.println("fail");
        }
        
        if (zabalenaLogika.test("musi to byt dlouhy")) {
            System.out.println("ok");
        } else {
            System.out.println("fail");
        }
    }
}
