package Perdicate.practical_using;

import java.util.function.Predicate;

public class Main {
  public static void main(String[] args) {
    predicateTest();
    predicateAndNegate();
    predicateIsEqual();
  }

  private static void predicateIsEqual() {
    Predicate<Integer> i1 = Predicate.isEqual(1);
    boolean test = i1.test(1);
    System.out.println(test);

    // ----------------

    String s1 = "hello";
    String s2 = "hello";

    Predicate<String> p1 = Predicate.isEqual(s1);
    test = p1.test(s2);
    System.out.println(test);
  }

  private static void predicateAndNegate() {
    Predicate<Integer> vetsi = (Integer number) -> {
      if (number > 13) {
        return true;
      } else {
        return false;
      }
    };

    Predicate<Integer> mensi = (Integer number) -> {
      if (number < 13) {
        return true;
      } else {
        return false;
      }
    };

    Predicate<Integer> desitka = (Integer number) -> {
      if (number == 10) {
        return true;
      } else {
        return false;
      }
    };

    int cislo = 10;
    System.out.println(mensi.and(vetsi).negate().test(cislo));
    System.out.println(mensi.and(desitka).test(cislo));
  }

  private static void predicateTest() {
    Predicate<Integer> vetsi = (Integer number) -> {
      if (number > 13) {
        return true;
      } else {
        return false;
      }
    };

    int cislo = 10;
    System.out.println(cislo + " je vetsi jak 13 ? " + vetsi.test(cislo));

    cislo = 15;
    System.out.println(cislo + " je vetsi jak 13 ? " + vetsi.test(cislo));
  }
}
