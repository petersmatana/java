package Perdicate;

public class Main {
  public static void main(String[] args) {
    basic();
//    andPredicate();
//    negate();
//    andNegate();
  }

  private static void andNegate() {
    MyPredicate<String> predicate = x -> {
      return true;
    };

    System.out.println(predicate.andNegate(predicate).negate().test("asd"));
  }

  private static void negate() {
    MyPredicate<String> predicate = x -> {
      return false;
    };

    System.out.println(predicate.negate().test("asd"));
  }

  private static void andPredicate() {
    MyPredicate<Integer> truePredicate = x -> {
      return true;
    };

    MyPredicate<Integer> falsePredicate = x -> {
      return false;
    };

    MyPredicate<Integer> test = truePredicate.and(truePredicate).and(truePredicate);
    System.out.println(test.test(10));
  }

  private static void basic() {
    MyPredicate<String> p1 = x -> {
      System.out.println("predicate = " + x);
      return true;
    };

    System.out.println(p1.test("asd"));

    MyPredicate<Integer> p2 = x -> {
      System.out.println("predicate 2 = " + x);

      if (x > 10) {
        System.out.println("vetsi");
        return true;
      } else {
        System.out.println("mensi");
        return false;
      }
    };

    p2.test(12);
    p2.test(1);
  }

  public static void testChain() {
    MyPredicate<Boolean> mujTest = p -> {
      return p;
    };

    boolean vysledek = mujTest
            .and(mujTest)
            .or(mujTest)
            .test(false);

    System.out.println("vysledek = " + vysledek);
  }
}
