package Perdicate;

import java.util.Objects;

@FunctionalInterface
public interface MyPredicate<T> {
  boolean test(T t);

  default MyPredicate<T> and(MyPredicate<T> other) {
    Objects.requireNonNull(other);

    return (T t) -> {
      return this.test(t) && other.test(t);
    };
  }

  default MyPredicate<T> or(MyPredicate<T> other) {
    Objects.requireNonNull(other);

    return (T t) -> {
      return this.test(t) || other.test(t);
    };
  }

  default MyPredicate<T> negate() {
    return (T t) -> {
      return !this.test(t);
    };
  }

  default MyPredicate<T> andNegate(MyPredicate<T> other) {
    Objects.requireNonNull(other);

    return (T t) -> {
      return this.test(t) && ! other.test(t);
    };
  }

  default MyPredicate<T> orNegate(MyPredicate<T> other) {
    Objects.requireNonNull(other);

    return (T t) -> {
      return this.test(t) || ! other.test(t);
    };
  }
}
