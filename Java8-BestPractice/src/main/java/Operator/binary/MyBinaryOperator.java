package Operator.binary;

import java.util.Comparator;

public class MyBinaryOperator {
    public static void main(String[] args) {
        IMyBinaryOperator<Integer> o = (x1, x2) -> {
            return x1 + x2;
        };
    
        Comparator<Integer> c = (x1, x2) -> {
            return x1 > x2 ? x1 : x2;
        };
    
        System.out.println(o.apply(1,1));
//        System.out.println(o.apply(c.compare(1,34)));
    }
}
