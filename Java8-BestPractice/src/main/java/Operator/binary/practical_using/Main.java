package Operator.binary.practical_using;

import java.util.function.BinaryOperator;

public class Main {
    public static void main(String[] args) {
        BinaryOperator<Integer> bo = (x1, x2) -> {
            return x1 > x2 ? x1 : x2;
        };
    
        System.out.println(bo.apply(1,3));
    }
}
