package Operator.binary;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;

@FunctionalInterface
public interface IMyBinaryOperator<T> extends BiFunction<T, T, T> {
    static <T> IMyBinaryOperator<T> maxBy(Comparator<T> compare) {
        return (a, b) -> {
            if (compare.compare(a, b) == 0) {
                return a;
            } else {
                return b;
            }
        };
    }
    
}
