package Operator.unary;

import java.util.function.Function;
import java.util.function.UnaryOperator;

@FunctionalInterface
public interface IMyUnaryOperator<T> extends Function<T, T> {
    default <T> UnaryOperator<T> identity(T t) {
        return (T x) -> {
            return t;
        };
    }
}


