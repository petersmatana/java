package Operator.unary.practical_using;

import java.util.function.UnaryOperator;

public class Main {
    public static void main(String[] args) {
        UnaryOperator<String> o = x -> {
            return x + "asd";
        };

        System.out.println(o.apply("x"));
    }
}
