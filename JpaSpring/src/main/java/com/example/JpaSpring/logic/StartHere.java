//package com.example.JpaSpring.logic;
//
//import com.example.JpaSpring.entity.Student;
//import com.example.JpaSpring.repository.StudentRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.DependsOn;
//
//import java.util.List;
//
//@Configuration
//public class StartHere {
//
//    private final StudentRepository studentRepository;
//
//    @Autowired
//    public StartHere(StudentRepository studentRepository) {
//        this.studentRepository = studentRepository;
//    }
//
//    @Bean("insertData")
//    public void basis() {
//        Student s1 = new Student("Miki", "S", "miki@email.com", 1);
//        Student s2 = new Student("Smonty", "S", "smonty@email.com", 333);
//        Student s3 = new Student("Jane", "S", "jane@email.com", 123);
//        this.studentRepository.saveAll(List.of(s1, s2, s3));
//
//        System.out.println("\n\nfind all students:");
//        this.studentRepository.findAll()
//            .stream()
//            .forEach(item -> System.out.println(item));
//    }
//
//    @Bean
//    public void findByEmail() {
//        System.out.println("\n\nfindByEmail:");
//        this.studentRepository.findStudentByEmail("kiki@email.com")
//            .stream()
//            .findAny()
//            .ifPresentOrElse(
//                    System.out::println,
//                    () -> System.out.println("can't find"));
//    }
//
//    @Bean
//    public void findByFirstNameAndAge() {
//        System.out.println("\n\nfindByFirstNameAndAge:");
//        this.studentRepository.findStudentsByFirstNameAndAgeIsGreaterThanEqual("Miki", 1)
//            .forEach(System.out::println);
//    }
//
//    @Bean
//    public void useJPQL() {
//        System.out.println("\n\nJPQL:");
//        this.studentRepository.jpaQuery("S")
//            .forEach(System.out::println);
//    }
//
//    @Bean
//    public void useSQL() {
//        System.out.println("\n\nSQL:");
//        this.studentRepository.sqlQuery("Miki", 1)
//            .forEach(System.out::println);
//    }
//
//    @Bean
//    @DependsOn({"insertData"})
//    public void delete() {
//        int rowsAfected = this.studentRepository.customDeleteStudentById(1L);
//        System.out.println("rows deleted = " + rowsAfected);
//    }
//}