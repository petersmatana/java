//package com.example.JpaSpring.logic;
//
//import com.example.JpaSpring.entity.Student;
//import com.example.JpaSpring.repository.StudentRepository;
//import com.github.javafaker.Faker;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.DependsOn;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Sort;
//
//import java.util.stream.IntStream;
//
//@Configuration
//public class SortAndPagination {
//
//    private final StudentRepository studentRepository;
//
//    @Autowired
//    public SortAndPagination(StudentRepository studentJpaRepository) {
//        this.studentRepository = studentJpaRepository;
//    }
//
//    @Bean("insertSampleData")
//    public void insertSampleData() {
//        Faker faker = new Faker();
//
//        IntStream.range(0, 50).forEach(insert -> {
//            final String firstName = faker.name().firstName();
//            final String lastName = faker.name().lastName();
//            final String email = String.format("%s.%s@company.com",
//                                               firstName.toLowerCase(),
//                                               lastName.toLowerCase());
//
//            Student s = new Student(
//                    firstName,
//                    lastName,
//                    email,
//                    faker.number().numberBetween(18,99)
//            );
//
//            this.studentRepository.save(s);
//        });
//    }
//
//    @Bean
//    @DependsOn({"insertSampleData"})
//    public void sortData() {
//        System.out.println("sort data:");
//
//        this.studentRepository
//            .findAll(Sort.by(Sort.Direction.ASC, "lastName"))
//            .forEach(item -> {
//                System.out.println("sort item = " + item);
//            });
//    }
//
//    @Bean
//    @DependsOn({"insertSampleData"})
//    public void sortByTwoAttributes() {
//        System.out.println("sort by two attributes");
//
//        Sort sort = Sort.by("firstName").descending()
//            .and(Sort.by("age").descending());
//        this.studentRepository.findAll(sort)
//            .forEach(System.out::println);
//    }
//
//    @Bean
//    @DependsOn({"insertSampleData"})
//    public void paginate() {
//        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by("firstName").ascending());
//        Page<Student> page = this.studentRepository.findAll(pageRequest);
//        page.get().forEach(item -> {
//            System.out.println("paginate = " + item);
//        });
//    }
//
//}