package com.example.JpaSpring.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    @DeleteMapping(path = {"/{studentId}"})
    public void deleteStudent(@PathVariable("studentId") Long id) {

    }

}