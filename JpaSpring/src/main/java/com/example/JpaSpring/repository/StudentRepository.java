package com.example.JpaSpring.repository;

import com.example.JpaSpring.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findStudentByEmail(String email);

    @Query("SELECT s FROM Student s WHERE s.lastName = ?1")
    List<Student> jpaQuery(String email);

    List<Student> findStudentsByFirstNameAndAgeIsGreaterThanEqual(String name, Integer age);

    @Query(value = "select * from student where first_name = :first_name and age >= :age", nativeQuery = true)
    List<Student> sqlQuery(
            @Param("first_name") String name,
            @Param("age") Integer age);

    @Transactional
    @Modifying
    @Query("delete from Student s where s.id = ?1")
    int customDeleteStudentById(Long id);

}
