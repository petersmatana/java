package com.example.JpaSpring.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Column;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;

@Entity(name = "StudentIdCard")
@Table(
    name = "student_id_card",
    schema = "public",
    uniqueConstraints = {
        @UniqueConstraint(
            name = "student_id_card_unique",
            columnNames = {"card_number"}
        )
    }
)
public class StudentIdCard {
    @Id
    @SequenceGenerator(
            name = "student_id_card_sequence",
            sequenceName = "student_id_card_sequence",
    allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "student_id_card_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;

    @Column(
            name = "card_number",
            nullable = false,
            length = 15
    )

    @OneToOne(
            cascade = CascadeType.ALL
//            fetch = FetchType.LAZY
//    mappedBy = "StudentIdCard"
    )
    @JoinColumn(
            name = "studen_id",
            referencedColumnName = "id"
    )
    @MapsId
//    @PrimaryKeyJoinColumn(
//            name = "student_id",
//            referencedColumnName = "id"
//    )
//    @PrimaryKeyJoinColumn
    private Student student;
    private String cardNumber;

    public StudentIdCard() {
    }

    public StudentIdCard(
            String cardNumber
            ) {
        this.cardNumber = cardNumber;
    }

    public Long getId() {
        return this.id;
    }

    public String getCardNumber() {
        return this.cardNumber;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public String toString() {
        return "StudentIdCard{" +
            "id=" + id +
            ", cardNumber='" + cardNumber + '\'' +
            '}';
    }
}