# Instalace

1. mam java home ```export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")```
2. stahnout binarku Maven z webu
3. rozbalit napr do adresare ~/Documents/
4. do ~/.bashrc vlozit ```export PATH=/home/smonty/Documents/<apache-maven-directory>/bin:$PATH```
5. pak staci v konzoli ```$ mvn```

# Pouzivani

## Zaklady
```
mvn archetype:generate \
-DgroupId=cesta.k.balicku \
-DartifactId=NazevProjektu \
-DarchetypeArtifactId=maven-archetype-quickstart \
-DinteractiveMode=false
```
kde vysledkem je tato adresarova struktura:

```.
   ├── pom.xml
   └── src
       ├── main
       │   └── java
       │       └── cesta
       │           └── k
       │               └── balicku
       │                   └── App.java
       └── test
           └── java
               └── cesta
                   └── k
                       └── balicku
                           └── AppTest.java   
```

`src/main/java` a `src/test/java` je vzdy stejne.
archtypeArtifactId je atribut kterym reknu podle jake sablony
se ma projekt vygenerovat. Moznosti jak to generovat jsou zde:
https://maven.apache.org/guides/introduction/introduction-to-archetypes.html

## J2EE projekt

```
mvn archetype:generate \
-DgroupId=cz.pkg \
-Dpackage=pkg \
-DartifactId=NazevProjektu \
-DarchetypeArtifactId=maven-archetype-j2ee-simple \
-DinteractiveMode=false
```

```
mvn archetype:generate \
-DgroupId=cesta.k.balicku \
-Dpackage=pkg \
-DartifactId=NazevProjektu \
-DarchetypeArtifactId=maven-archetype-webapp \
-DinteractiveMode=false
```

## Dalsi atributy v XML pom.xml
|Nazev|Popis|
|----|----|
|modelVersion|Urcuje pouze verzi XML Mavenu. V root tagu \<project> je uvedena verze XML. Typicky je tam verze 4.0.0.|
|groupId|Urcuje cestu balicku. V tomto pripade je to cesta.k.balicku.|

## Zakladni zivotni cyklus
Jsou to faze: default, clean a site. Dalsi jsou: validate, compile, test,
package, verify, install, deploy.

# verify
nic moc nerikajici: run any checks to verify the package is valid and meets quality criteria

# install
nainstaluje novou zavislost
