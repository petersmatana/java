# alternatives

`alternatives --list` vypise vsechny veci ktery muzu nastavovat. vypisuje se jmeno a cesta..

`alternatives --display <name>` vypise podrobnosti. napr name ~ java

# sdkman

## Instalace sdkman

https://sdkman.io/install

1. instaluju bez roota, prikaz: `curl -s "https://get.sdkman.io" | bash` 
2. `source "$HOME/.sdkman/bin/sdkman-init.sh"` - vhodne zkopirovat do ~/.bashrc
3. `sdk version`

## Pouzivani - online mode

`sdk offline disable`

`sdk list java` seznam java verzi k dispozici

## Pouzivani - offline mode

`sdk offline enable`

`sdk list java` seznam nainstalovanych a pouzivanych java verzi

syntaxe instalovani javy v dane verzi od daneho vendora. `sdk install java <version>-<vendor>` priklad: `sdk install java 8.0.232.hs-adpt` pripadne je tam sloupec **Identifier**.

prepinani mezi verzema javy. `sdk use java <Identifier>`

# instalace Kotlin

## sdkman

1. `$ sdk install kotlin`

## intellij

Defakto bezproblemu se v nastaveni v kompilatoru kotlin objevi to co jsem instaloval pomoci sdkman.

**Tvorba noveho projektu v IntelliJ** je jednoduche. File -> New -> Project pak Kotlin -> JVM/IDEA a naklikam projekt.
