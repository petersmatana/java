# Java 8

## promenna v interface

od 8 a vyse mohu udelat v interface toto:
```
interface I {
    String text = "value";
}
```

toto je ekvivalentni:

```
interface I {
    public static final String text = "value";
}
```

## @FunctionalInterface

FunctionalInterface ma pouze jednu abstraktni metodu.

## Iterator - forEachRemaining

umi zprocesovat napr celou kolekci

# Java 9

## privatni metody v interface

od 9 a vyse mohu udelat v interface toto:
```
interface I {
    private void metoda() {

    }
}
```

# Java 11

## java.utils.function

### Predicate
`Predicate.not()`

### Optional

`Optional.isEmpty()`

# Java 10

## java.utils.function

### Optional

`Optional.orElseThrow()`

# install

## maven
do ~/.bashrc pridam:
```
M2_HOME=/cesta/do/adresare/s/mavenem
export M2_HOME
PATH=$PATH:$M2_HOME/bin
export PATH
```

# Spring

## application.properties file

`application.properties` is default config file.

Env file can be chosen by argument: `--spring.profile.active=dev`

# Spring Anotace

## Component
Annotated class with `@Component` make this class as a bean which behaves as singleton. (There are different scopes - prototype, request, session)

Also `@Component` can have value like this: `@Component(value = "MySpecialComponent")` which can be inject like this:

```
private final MyComponent myComponent;

@Autowired
public SomeServiceConstructor(@Qualifier("MySpecialComponent") MyComponent myComponent) {
    this.myComponent = myComponent;
}
```

## Service & Repository
Do the same thing like `@Component` but is more precisely - handling business / service logic. `@Repository` dealing with data.

## Bean & Configuration
Method commandLineRunner is run in the start of app.

```
@Configuration
public class CustomerConfiguration {

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> {
            System.out.println("hello from @Bean");
        };
    }
}
```

## Value
With this I can pass some argument to app like this:
`java -jar path/to/app.jar --app.mySwitcher=true --another.variable=value`

Name have to be same. In this case *mySwitcher*.

```
@Value("${app.mySwitcher:false}") # false is default value
private Boolean mySwitcher;
```

## Controller
Controller is special type of Component annotation - in context of Spring MVC this returns like jsp page. If I would like to use `@Controller` in context of return JSON data via RESTful I have to do this:

```
@Controller
public class OldController {

    @GetMapping("/number")
    public ResponseEntity<Integer> getNumber() {
        return ResponseEntity.status(HttpStatus.OK).body(42);
    }

    @ResponseBody
    @GetMapping("/string")
    public String getString() {
        return "hello";
    }
}
```

Method have to return `ResponseEntity` with data type of object. Or I should use annotation `@ResponseBody`. Annotation `@ResponseBody` can by used on class level so all method in class will be wrapped with `ResponseEntity` type.

## RestController
Returns data in way of RESTful API. Do not need to use `@ResponseBody` or `ResponseEntity`. This annotation do this by default so I can do this:

```
@RestController
public class CustomerController {

    @GetMapping(value = "/customer")
    public List<Customer> getCustomer() {
        return Collections.emptyList();
    }
}
```

## RequestMapping

All endpoints in class CustomerController can have prefix done like this:

```
@RequestMapping("/api/v1/customer")
public class CustomerController {
...
}
```

## Primary

// todo

## Deprecated

Class / method is old and should not be using any more.

## ConfigurationProperties

This is like converting properties and its values into class.

# Testing

## ExtendWith
Anotace `@ExtendWith({Trida1.class, Trida2.class})` se pouziva kdyz delam JUnit testy a pridavam tim do tridy instanci jinych trid. Napriklad pridam logovani, repozitory nebo service...
mohu pridavat tridy, ktere dedi(extends) z org.junit.jupiter.api.extension.Extension
https://www.baeldung.com/junit-5-extensions#register-extensions

## SpringBootTest
Anotace `@SpringBootTest` dela autokonfiguraci zavislostem: junit-jupiter:5.4.0 a spring-boot-starter-test.

## ContextConfiguration
Anotace `@ContextConfiguration` nacte springovej ApplicationContext pro ucely integracniho testovani.

## Disabled.
Anotace ktera zakaze spousteni daneho testu v ramci cele tridy. `@Disabled` se nemuze kombinovat s anotaci `@Test`.

# Spring DI

Never do this:
```
@RestController
public class C {

    private MyService myService;

    public C() {
        myService = new MyService();
    }

}
```
because this code can be hardly tested.

Batter approach:

```
@RestController
public class C {

    private final MyService myService;

    @Autowired
    public C(MyService service) {
        this.myService = service;
    }

}
```

# Entity serialization

Serialized is all what match this method name: getX where x is any string. To serialization goes only `getId()` and `getCustomString()`.

```
public class Customer {
    private final Long id;
    private final String name;

    ...constructor

    public Long getId() {
        return id;
    }

    public String getCustomString() {
        return "hard text";
    }

    public String thisWontTake() {
        return "no";
    }

    ...toString
}
```

## JsonIgnore

Annotation `@JsonIgnore` upon get method ignore this field in final serialization.

```
@JsonIgnore
public Double getPi() {
    return 3.14;
}
```

## JsonProperty

There is possible to change name like this:

```
@JsonProperty(value = "overload_name")
public Long getId() {
    return id;
}
```

----

# MAVEN



tahak: jak spoustet app: mvn clean package spring-boot:run


# TODO

https://github.com/OpenFeign/feign
