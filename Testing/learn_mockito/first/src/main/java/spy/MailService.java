package spy;

public interface MailService {
    void sendEmail(String msg);
    boolean hasSentMessage(String msg);
}
