package spy;

import java.util.ArrayList;
import java.util.List;

// stub dela to, ze nad nim muzu volat "preddevinovane odpovedi"
public class MailServiceStub implements MailService {

    private List<String> messages = new ArrayList<>();

    @Override
    public void sendEmail(String msg) {
        messages.add(msg);
    }

    @Override
    public boolean hasSentMessage(String msg) {
        return messages.contains(msg);
    }
}
