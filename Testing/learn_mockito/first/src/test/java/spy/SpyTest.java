package spy;

import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpyTest {
    private static MailServiceStub mss;
    private static MailService service;

    public static void main(String[] args) {
        setUp();

        jedenTest();
        druhyTest();
        assertionTest();
    }

    public static void setUp() {
        mss = new MailServiceStub();
    }

    public static void jedenTest() {
        service = mock(MailService.class);

        mss.sendEmail("hello");

        when(service.hasSentMessage("hello")).then(invocation -> {
            Assertions.assertSame(invocation, "hello");
            return true;
        });
    }

    public static void druhyTest() {
        // todo, nevim proc tohle nemuzu resit v setUp
        service = mock(MailService.class);

        mss.sendEmail("hello");

        when(service.hasSentMessage("hello")).then(invocation -> {
            Assertions.assertNotSame(invocation, "asd");
            // todo, tohle stejne nepada takze nevim no :/
            return true;
        });
    }

    public static void assertionTest() {
        mss.sendEmail("hello");

        Assertions.assertSame(mss.hasSentMessage("hello"), true);

        // toto spadne ale bojim se ze s tim Mockito nema moc co spolecne :D
//        Assertions.assertSame(mss.hasSentMessage("hellooo"), true);
    }
}
