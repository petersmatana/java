package mock;

import org.mockito.stubbing.Answer;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

public class PortfolioTest {
    private static Portfolio portfolio;
    private static StockService stockService;

    public static void main(String[] args) {
        PortfolioTest test = new PortfolioTest();
        test.setUp();

        testMarketValue();
    }

    public static void setUp() {
        portfolio = new Portfolio();
        stockService = mock(StockService.class);
        portfolio.setStockService(stockService);
    }

    public static void testMarketValue(){
        // given
        List<Stock> stocks = new ArrayList<>();
        Stock googleStock = new Stock("1","Google", 10);
        Stock microsoftStock = new Stock("2","Microsoft",100);

        stocks.add(googleStock);
        stocks.add(microsoftStock);

        portfolio.setStocks(stocks);

        // when / then
        when(stockService.getPrice(googleStock)).thenReturn(50.00);

        // todo, test "probehne" a nic se nestane
        when(stockService.getPrice(googleStock)).thenReturn(60.00);
        when(stockService.getPrice(microsoftStock)).thenReturn(1000.00);

        when(portfolio.getMarketValue()).thenReturn(100500.0);
    }

}
