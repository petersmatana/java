package cz.pkg.repository;

import cz.pkg.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    Optional<Product> findProductByIdAndName(Integer id, String name);
}
