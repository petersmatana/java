package cz.pkg.integraftionTesting;

import cz.pkg.model.Product;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTests2 {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int randomServerPort;

    @Test
    public void addProduct() throws URISyntaxException, JSONException {
        final String baseUrl = "http://localhost:" + this.randomServerPort + "/products";
        URI uri = new URI(baseUrl);
        Product product = new Product("name", "description", 13);
        String expectedProduct = "{\"id\": 1,\"name\": \"name\",\"description\": \"description\",\"quantity\": 13,\"version\": 0}";

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-COM-PERSIST", "true"); // todo - co to je?

        HttpEntity<Product> request = new HttpEntity<>(product, httpHeaders);

        ResponseEntity<String> response = this.testRestTemplate.postForEntity(uri, request, String.class);

        // tests
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        // jsem zmaten, vraci to string...?
//        Assert.assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getHeaders().getContentType());

        JSONAssert.assertEquals(expectedProduct, response.getBody(), false);
    }

    @Test
    public void record2NotExist() throws URISyntaxException {
        final String baseUrl = "http://localhost:" + this.randomServerPort + "/products/2";
        URI uri = new URI(baseUrl);

        ResponseEntity<String> response = this.testRestTemplate.getForEntity(uri, String.class);

        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertEquals("Product with ID: 2 can not be found.", response.getBody());
    }
}
