package cz.pkg.integraftionTesting;

import cz.pkg.model.Product;
import cz.pkg.repository.ProductRepository;
import cz.pkg.service.ProductService;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class IntegrationTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private ProductRepository productRepository;

    @LocalServerPort
    private int randomPort;

    @Test
    public void testService() throws JSONException {
        Product product = new Product("name", "description", 13);
        String expectedProduct = "{\"id\": 1,\"name\": \"name\",\"description\": \"description\",\"quantity\": 13,\"version\": 0}";

        Mockito.when(productRepository.findById(1)).thenReturn(Optional.of(product));

        ResponseEntity<String> response = testRestTemplate.getForEntity("/products/1", String.class);

        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        // service mi vraci text, controller mi vraci json...
        Assert.assertEquals(MediaType.TEXT_PLAIN, response.getHeaders().getContentType());

        JSONAssert.assertEquals(expectedProduct, response.getBody(), false);

        Mockito.verify(productRepository, Mockito.times(1)).findById(1);
    }
}
