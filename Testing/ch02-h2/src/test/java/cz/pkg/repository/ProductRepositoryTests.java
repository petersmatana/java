package cz.pkg.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.pkg.model.Product;
import cz.pkg.service.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class ProductRepositoryTests {

    private static final Logger logger = LogManager.getLogger(ProductService.class);

    private static File DATA_JSON = Paths.get("src", "test", "resources", "products.json").toFile();

    @Autowired
    private ProductRepository productRepository;

    @BeforeEach
    public void setup() throws IOException {
        // Deserialize products from JSON file to Product array
        Product[] products = new ObjectMapper().readValue(DATA_JSON, Product[].class);

        // Save each product to database
        Arrays.stream(products).forEach(productRepository::save);
    }

    @AfterEach
    public void cleanup() {
        productRepository.deleteAll();
    }

    @Test
    public void testMustPass() {
        Assertions.assertTrue(true);
    }

    @Disabled(value = "Toto je jen testovaci test, ktery failuje. Anotace @Disabled se nemuze kombinovat s anotaci @Test")
    public void testMustFail() {
        Assertions.assertTrue(false);
    }

    @Test
    @DisplayName("Test product not found with non-existing id")
    public void testProductNotFoundForNonExistingId() {
        int id = 3;
        // given two products in db by setup

        // when
        Optional<Product> retrievedProduct = productRepository.findById(id);

        // then
        Assertions.assertEquals(Optional.empty(), retrievedProduct);
    }

    @Test
    @DisplayName("Test first product.")
    public void testFirstItem() throws IOException {
        // given
        Product[] products = new ObjectMapper().readValue(DATA_JSON, Product[].class);
        productRepository.save(products[0]);

        // when
        Optional<Product> retrievedProduct = productRepository.findById(1);

        // then
        Assertions.assertEquals(retrievedProduct.get().toString(), products[0].toString());
    }

}
