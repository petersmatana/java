package cz.pkg.service;

import cz.pkg.model.Product;
import cz.pkg.repository.ProductRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.AssertionErrors;

import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductServiceTests {
    @Autowired
    private ProductService productService;

    @MockBean
    private ProductRepository productRepository;

    @Test
    @DisplayName("Find product with id successfully")
    public void testFindProductById() {
        Product mockProduct = new Product(1, "Product", "Description", 10, 1);

        doReturn(mockProduct).when(productRepository).findProductById(1);

        Product foundProduct = productService.findById(1);

        Assertions.assertNotNull(foundProduct);
        Assertions.assertSame("Product", foundProduct.getName());
    }

    @Test
    public void testSuccessfullySaveProduct() {
        final String name = "this is name";
        final String description = "this is description";
        final Integer quantity = 13;
        Product mockProduct = new Product(name, description, quantity);

        doReturn(mockProduct).when(productRepository).save(any());

        Product saveProduct = productService.saveProduct(mockProduct);

        AssertionErrors.assertNotNull("This should not be null.", saveProduct);
        Assertions.assertSame(saveProduct.getName(), name);
        Assertions.assertSame(saveProduct.getDescription(), description);
        Assertions.assertSame(saveProduct.getQuantity(), quantity);
    }
}
