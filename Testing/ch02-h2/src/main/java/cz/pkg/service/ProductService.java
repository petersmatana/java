package cz.pkg.service;

import cz.pkg.model.Product;
import cz.pkg.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ProductService {
    private static final Logger logger = LogManager.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;

    @Transactional
    public Product saveProduct(Product product) {
        logger.info("Saving new product: ", product.toString());
        return productRepository.save(product);
    }

    public Product findById(Integer id){
        logger.info("Finding product by id: ", id);
        Product product = productRepository.findProductById(id);
        if (product != null) {
            return product;
        } else {
            return null;
        }
    }

    public void updateProduct(Product product) {
        logger.info("Updating product: ", product.toString());
        Object existingProduct = productRepository.findById(product.getId());
        if (existingProduct != null) {
            Product tmp = (Product) existingProduct;
            System.out.println(tmp.toString());
        } else {
            logger.info("Can NOT update existing product: ", product.toString());
        }
    }
}
