package cz.pkg.repository;

import cz.pkg.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    Product findProductById(Integer id);
    Optional<Product> findProductByIdAndName(Integer id, String name);
}
