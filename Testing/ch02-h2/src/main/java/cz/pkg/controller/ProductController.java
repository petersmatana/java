package cz.pkg.controller;

import cz.pkg.model.Product;
import cz.pkg.service.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class ProductController {
    private static final Logger logger = LogManager.getLogger(ProductService.class);

    @Autowired
    private ProductService productService;

    @PostMapping("/products")
    public ResponseEntity<?> createProduct(@RequestBody Product product) {
        logger.info("Try to create new Product: " + product.toString());
        Product newProduct = productService.saveProduct(product);
        try {
            return ResponseEntity
                    .created(new URI("/products/" + newProduct.getId()))
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(newProduct);
        } catch (URISyntaxException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer id) {
        logger.info("Try to find Product with ID: " + id);
        Product product = productService.findById(id);
        try {
            if (product != null) {
                return ResponseEntity
                        .created(new URI("/products/" + id.toString()))
                        .body(product);
            } else {
                return ResponseEntity
                        .created(new URI("/notFound"))
                        .body("Product with ID: " + id.toString() + " can not be found.");
            }
        } catch (URISyntaxException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
