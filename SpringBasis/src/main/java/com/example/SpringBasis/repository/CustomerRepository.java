package com.example.SpringBasis.repository;

import com.example.SpringBasis.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository(value = "real")
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}