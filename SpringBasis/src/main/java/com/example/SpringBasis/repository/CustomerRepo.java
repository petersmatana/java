package com.example.SpringBasis.repository;

import com.example.SpringBasis.entity.Customer;
import java.util.List;

public interface CustomerRepo {
    List<Customer> getCustomers();

    void insertCustomer(Customer customer);
}