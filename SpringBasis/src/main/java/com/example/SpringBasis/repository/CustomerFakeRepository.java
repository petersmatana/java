package com.example.SpringBasis.repository;

import com.example.SpringBasis.entity.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Repository(value = "fake")
public class CustomerFakeRepository implements CustomerRepo {

    private List<Customer> data;

    public CustomerFakeRepository() {
        this.data = new LinkedList<>();

        this.data.add(new Customer(1L, "James Bond"));
        this.data.add(new Customer(2L, "Kozel Bobes"));
    }

    @Override
    public List<Customer> getCustomers() {
        return this.data;
    }

    @Override
    public void insertCustomer(@Validated Customer customer) {
        this.data.add(customer);
    }
}