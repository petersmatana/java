package com.example.SpringBasis.exception;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public class ApiException {
    private final String message;
    private final Throwable throwable;
    private final HttpStatus httpStatus;
    private final ZonedDateTime zonedDateTime;

    public ApiException(
            String message,
            Throwable throwable,
            HttpStatus httpStatus,
            ZonedDateTime zonedDateTime
            ) {
        this.message = message;
        this.throwable = throwable;
        this.httpStatus = httpStatus;
        this.zonedDateTime = zonedDateTime;
    }

    public String getMessage() {
        return this.message;
    }

    public Throwable getThrowable() {
        return this.throwable;
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

    public ZonedDateTime getZonedDateTime() {
        return this.zonedDateTime;
    }

    @Override
    public String toString() {
        return "ApiException{" +
            "message='" + message + '\'' +
            ", throwable=" + throwable +
            ", httpStatus=" + httpStatus +
            ", zonedDateTime=" + zonedDateTime +
            '}';
    }
}