package com.example.SpringBasis.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.validation.constraints.NotEmpty;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class Customer {

//    @NotEmpty(message = "id must be set")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "name must be set")
    private String name;

    public Customer() {
    }

    public Customer(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @JsonProperty(value = "overload_name")
    public Long getId() {
        return this.id;
    }

    public String getCustomString() {
        return "na tvrdo";
    }

    @JsonIgnore
    public Double getPi() {
        return 3.14;
    }

    @JsonProperty
    public String thisWontTake() {
        return "no";
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Customer{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }
}