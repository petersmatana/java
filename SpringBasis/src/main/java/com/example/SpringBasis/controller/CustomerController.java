package com.example.SpringBasis.controller;

import com.example.SpringBasis.entity.Customer;
import com.example.SpringBasis.exception.ApiRequestException;
import com.example.SpringBasis.service.CustomerService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(value = "/customer")
    @Deprecated
    public List<Customer> getCustomer() {
        return this.customerService.getCustomer();
    }

    @GetMapping
    public List<Customer> getAllCustomers() {
        return this.customerService.getCustomer();
    }

    @GetMapping(path = {"/{customerId}"})
    public Customer getCustomerById(@PathVariable Long customerId) {
        return this.customerService.getCustomerById(customerId);
    }

    @GetMapping(path = {"/{customerId}/exception"})
    public Customer getCustomerException(@PathVariable Long customerId) {
        throw new ApiRequestException("ApiRequestException for customer " + customerId);
    }

    @PostMapping
    public String insertCustomer(@Valid @RequestBody Customer customer) {
        this.customerService.insertCustomer(customer);
        return "customer " + customer.toString() + " was created";
    }

    @PutMapping
    public void updateustomer(@RequestBody Customer customer) {
        System.out.println("update customer");
    }

    @DeleteMapping(path = {"/{customerId}"})
    public void deleteCustomer(@PathVariable("customerId") Long id) {
        System.out.println("delete customer with id = " + id);
    }
}