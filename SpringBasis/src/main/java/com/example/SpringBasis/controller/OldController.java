package com.example.SpringBasis.controller;

import com.example.SpringBasis.service.OldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

@Controller
@RequestMapping("/api")
public class OldController {

    private final OldService oldService;

    @Autowired
    public OldController(OldService oldService) {
        this.oldService = oldService;
    }

    @GetMapping("/number")
    public ResponseEntity<Integer> getNumber() {
        return ResponseEntity.status(HttpStatus.OK).body(42);
    }

    @ResponseBody
    @GetMapping("/string")
    public String getString() {
        return "hello";
    }

    @GetMapping("/manyArgs/{text}/{number}")
    public ResponseEntity<String> manyArguments(
            @PathVariable String text,
            @PathVariable int number
            ) {
        return ResponseEntity.status(HttpStatus.OK)
            .body("two args - text = " + text + ", number = " + number);
    }

    @GetMapping(value = {"/optional", "/optional/{text}/{number}"})
    @ResponseBody
    public String optionalPathArgument(
            @PathVariable(required = false) Optional<String> text,
            @PathVariable(required = false) Optional<Integer> number
            ) {
        return this.oldService.optionalPathArgument(text, number);
    }
}