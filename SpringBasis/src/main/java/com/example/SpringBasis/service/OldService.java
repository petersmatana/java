package com.example.SpringBasis.service;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OldService {
    public String optionalPathArgument(Optional<String> text, Optional<Integer> number) {
        StringBuilder sb = new StringBuilder();
        sb.append("optional args - ");

        if (text.isPresent()) {
            sb.append("text = " + text.get() + ", ");
        } else {
            sb.append("text is not present, ");
        }

        if (number.isPresent()) {
            sb.append("number - " + number.get());
        } else {
            sb.append("number is not present");
        }

        return sb.toString();
    }
}