package com.example.SpringBasis.service;

import com.example.SpringBasis.entity.Customer;
import com.example.SpringBasis.exception.NotFoundException;
import com.example.SpringBasis.repository.CustomerRepo;
import com.example.SpringBasis.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getCustomer() {
        LOGGER.debug("get customer");
        return this.customerRepository.findAll();
    }

    public void insertCustomer(Customer customer) {
        this.customerRepository.save(customer);
    }

    public Customer getCustomerById(Long id) {
        return this.customerRepository.findAll()
            .stream()
            .filter(customer -> customer.getId().equals(id))
            .findFirst()
            .orElseThrow(() -> {
                String message = "customer with ID " + id + " not found";
                LOGGER.error(message);
                return new NotFoundException(message);
            });
    }
}