package com.example.SpringBasis.configuration;

import com.example.SpringBasis.repository.CustomerFakeRepository;
import com.example.SpringBasis.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CustomerConfiguration {

    @Value("${app.useFakeCustomerRepo:false}")
    private Boolean useFakeCustomerRepo;

    @Value("${app.number:42}")
    private int number;

    private final Environment environment;

    private AppInfo appInfo;

    @Autowired
    public CustomerConfiguration(
            Environment environment,
            AppInfo appInfo) {
        this.environment = environment;
        this.appInfo = appInfo;
    }

    @Bean
    public void setEnv() {
        this.environment.getProperty("app.env");
        System.out.println("env is = " + this.environment);
    }

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> {
            System.out.println("hello from @Bean");
        };
    }

    @Bean
    public CustomerRepo customerRepo() {
        System.out.println("value of useFakeCustomerRepo = " + this.useFakeCustomerRepo);
        return new CustomerFakeRepository();
    }

    @Bean
    public void numberInfo() {
        System.out.println("number is = " + number);
    }

    @Bean
    public void printEnv() {
        System.out.println(this.appInfo);
    }
}