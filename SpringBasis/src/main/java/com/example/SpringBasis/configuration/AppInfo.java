package com.example.SpringBasis.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
public class AppInfo {
    private int number;
    private String env;

    public int getNumber() {
        return this.number;
    }

    public String getEnv() {
        return this.env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "AppInfo{" +
            "number=" + number +
            ", env='" + env + '\'' +
            '}';
    }
}