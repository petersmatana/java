package cz.pkg.java8interfaces;

public interface MyInterface {
  public static final String text = "asd";

  private void moznoOd9() {

  }

  void obycejnaMetodaInterface();

  default void defaultMetoda() {
    System.out.println("interface - default metoda");
  }

  default MyInterface defaultMetodaSNavratem() {
    System.out.println("interface - default metoda s navratem");

    return MyInterface::statickaMetoda;
  }

  static void statickaMetoda() {
    System.out.println("interface - staticka metoda");
  }

  static MyInterface statickaMetodaSNavratem() {
    System.out.println("interface - staticka metoda co vraci MyInterface");

    // tak tohle je peklo
//    return MyInterface::dalsiStatickaMetodaSNavratem;
    return null;
  }

  static MyInterface dalsiStatickaMetodaSNavratem() {
    System.out.println("interface - dalsi staticka metoda co vraci MyInterface");

    return MyInterface::statickaMetodaSNavratem;
  }
}
