package cz.pkg.java8interfaces;

public class Main {
  public static void main(String[] args) {
//    begin();
//    abstraktniTrida();
//    statickyKontextInterface();
    neniToFncInterface();
  }

  public static void neniToFncInterface() {
    MyInterface i = () -> {};

    i.defaultMetodaSNavratem().obycejnaMetodaInterface();
  }

  public static void statickyKontextInterface() {
    MyInterface.statickaMetoda();
    MyInterface i = MyInterface.statickaMetodaSNavratem();
    MyInterface i2 = MyInterface.dalsiStatickaMetodaSNavratem();

    MyInterface.dalsiStatickaMetodaSNavratem();
  }

  public static void abstraktniTrida() {
    AbstraktniTrida at = new AbstraktniTrida() {
      @Override
      public void test() {
        System.out.println("test");
      }
    };

    at.test();
  }

  public static void begin() {
    MyClass trida = new MyClass();
    trida.obycejnaMetodaInterface();
    trida.defaultMetoda();

    MyInterface.statickaMetoda();
  }
}
