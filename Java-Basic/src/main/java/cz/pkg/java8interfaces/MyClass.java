package cz.pkg.java8interfaces;

public class MyClass implements MyInterface {
  @Override
  public void obycejnaMetodaInterface() {
    System.out.println("implementace - obycejna metoda");
  }

  @Override
  public void defaultMetoda() {
    System.out.println("implementace - default metoda");
  }
}
