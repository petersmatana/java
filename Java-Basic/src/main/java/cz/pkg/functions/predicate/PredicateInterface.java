package cz.pkg.functions.predicate;

import java.util.function.Predicate;

public class PredicateInterface {

  public static void begin() {
    Predicate<Integer> p = (Integer i) -> {
      if (i > 13) {
        return true;
      } else {
        return false;
      }
    };

    // kratsi zapis
    Predicate<Integer> p2 = x -> x > 13;

    if (p2.test(100)) {
      System.out.println("ano");
    } else {
      System.out.println("ne");
    }
  }

  /*pass predicate as argument
  * */
  public static void test(Predicate<Integer> p, Integer arg) {
    if (p.test(arg)) {
      System.out.println("ano");
    } else {
      System.out.println("ne");
    }
  }

  public static void main(String[] args) {
//    begin();
  }
}
