package cz.pkg.functions.predicate;

import java.util.function.BiPredicate;
import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.LongPredicate;

public class SpecializedPredicate {
  public static void main(String[] args) {
    biPredicate();
  }

  /*dalsi predicate z verze 8 je BiPredicate
  * */
  private static void biPredicate() {
    Integer size = 100;
    String text = "hello world";

    BiPredicate<Integer, String> p = (Integer i, String s) -> {
      if ((i > 10) && s.length() > 0) {
        return true;
      } else {
        return false;
      }
    };

    System.out.println(p.test(size, text));
  }

  /*java 8 zavadi tyto "specializovane"
  predicate.
  * */
  private static void numericPredicates() {
    IntPredicate intPredicate;
    LongPredicate longPredicate;
    DoublePredicate doublePredicate;
  }
}
