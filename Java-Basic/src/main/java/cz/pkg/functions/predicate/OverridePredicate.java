package cz.pkg.functions.predicate;

import java.util.Objects;
import java.util.function.Predicate;

/*ukolem je zjistit jestli string zacina
znakem "a" a ma delku vetsi nez 4
* */
public class OverridePredicate {
  public static void main(String[] args) {
//    begin();
    override();
  }

  private static void override() {
    Predicate<String> p = new Predicate<String>() {
      @Override
      public boolean test(String s) {
        Objects.requireNonNull(s);
        if ((s.length() >= 4) && (s.charAt(0) == 'a')) {
          return true;
        } else {
          return false;
        }
      }
    };

    System.out.println(p.test("ahoj"));
    System.out.println(p.test("ne"));
  }

  private static void begin() {
    Predicate<String> length = (String s) -> {
      if (s.length() >= 4) {
        return true;
      } else {
        return false;
      }
    };

    Predicate<String> charA = (String s) -> {
      if (s.charAt(0) == 'a') {
        return true;
      } else {
        return false;
      }
    };

    System.out.println(length.and(charA).test("alpha"));
    System.out.println(length.and(charA).test("aha"));
    System.out.println(length.and(charA).test("nenene"));
  }
}
