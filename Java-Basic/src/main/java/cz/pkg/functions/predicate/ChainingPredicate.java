package cz.pkg.functions.predicate;

import java.util.function.Predicate;

public class ChainingPredicate {
  public static void main(String[] args) {
//    begin();
    next();
  }

  private static void next() {
    String s1 = "hello";
    String s2 = "Hello";

    Predicate<String> p = Predicate.isEqual(s1);
    if (p.negate().test(s2)) {
      System.out.println("equal");
    } else {
      System.out.println("! equal");
    }

    if (p.test(s2)) {
      System.out.println("equal");
    } else {
      System.out.println("! equal");
    }
  }

  private static void begin() {
    Predicate<Integer> p1 = x -> x < 7;
    Predicate<Integer> p2 = x -> x > 3;
    Predicate<Integer> p3 = x -> x%2 == 0;

    System.out.println("5 = " + p1.and(p2).test(5));
    System.out.println("9 = " + p1.and(p2).test(9));

    System.out.println(((Predicate<Integer>)(x->x==x)).test(0));
  }
}
