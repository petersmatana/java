package cz.pkg.functions.use_in_optional;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Supplier;

public class Begin {
	public static void main(String[] args) {
		test();
		
//		chaining1();
//		chaining2();
		
//		isEmpty();
//		isPresent();
//		begin();
	}
	
	private static void test() {
		Supplier<Optional<String> > supplier = () -> {
		    System.out.print("Enter a string:");
		    return Optional.of((new Scanner(System.in)).nextLine());
		};
		String s = null;
		Optional<String> os = Optional.ofNullable(s)
		                              .or(supplier);
		if (os.isPresent())
		    System.out.println(os.get());
	}
	
	private static void chaining2() {
		String t = null;
		String s = "asd";
		
		String s2 = Optional
				.ofNullable(t)
				.orElse(s);
		System.out.println(s2);
		
		String s3 = null;
		try {
			s3 = Optional
					.ofNullable(t)
					.orElseThrow(() -> new Exception("exception"));
		} catch (Exception ex) {
			System.out.println("chyba");
		}
		System.out.println(s3);
	}
	
	/* ofNullable() pokud neni null, vraci pozadovanou hodnotu
	 * jinak vraci empty()
	 * */
	private static void chaining1() {
		String s1 = null;
		String s2 = "asd";
		
		String s = Optional.ofNullable(s1).orElse(s2);
		
		System.out.println(s);
	}
	
	/* isEmpty() vraci true, pokud Optional
	 * obsahuje null objekt
	 * */
	private static void isEmpty() {
		Optional<String> o = Optional.ofNullable(null);
		
		if (o.isEmpty()) {
			System.out.println("o is empty");
		} else {
			System.out.println("o is not empty");
		}
	}
	
	/* isPresent() vraci true, pokud Optional obsahuje
	 * non-null objekt. jinak false
	 * */
	private static void isPresent() {
		Optional<String> o = Optional.empty();
		
		if (o.isPresent()) {
			System.out.println("o is present");
		} else {
			System.out.println("o is not present");
		}
		
		o = Optional.of("asd");
		
		if (o.isPresent()) {
			System.out.println("o is present");
		} else {
			System.out.println("o is not present");
		}
	}
	
	private static void begin() {
		try {
			Optional<String> o1 = Optional.of(null);
		} catch (NullPointerException ex) {
			System.out.println("null pointer excetion");
		}
		
		Optional<String> o2 = Optional.of("asd");
		System.out.println(o2.get());
		
		Optional<String> o3 = Optional.ofNullable("asd");
		System.out.println(o3.get());
		
		Optional<String> o4 = Optional.ofNullable(null);
		try {
			System.out.println(o4.get());
		} catch (NoSuchElementException ex) {
			System.out.println("no such element exception");
		}
		
		Optional<String> o5 = Optional.empty();
		try {
			System.out.println(o5.get());
		} catch (NoSuchElementException ex) {
			System.out.println("no such element excetion");
		}
	}
}
