package cz.pkg.functions.use_is_collections;

import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;

public class ReplaceElements {

	public static void main(String[] args) {
		List<Double> list = Arrays.asList(76.0,627.0,2445.0,7346.0,23.0,6532.0,523.0,745.0,745.0);
		
		UnaryOperator<Double> div = (Double number) -> {
			return number / 4;
		};
		
		for (Double number : list) {
			System.out.println("origin = " + number);
		}
		
		list.replaceAll(div);
		
		for (Double number : list) {
			System.out.println("after replace = " + number);
		}
	}
	
}
