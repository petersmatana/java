package cz.pkg.functions.use_is_collections;

import java.util.ArrayList;
import java.util.function.Predicate;

public class Remove {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Super");
		list.add("Random");
		list.add("Silly");
		list.add("String");
		
		Predicate<String> delete = (String s) -> {
			if (s.startsWith("R")) {
				return true;
			} else {
				return false;
			}
		};
		
		list.removeIf(delete);
		
		list.forEach((String s) -> {
			System.out.println(s);
		});
	}
	
}
