package cz.pkg.functions.use_is_collections;

import java.util.Arrays;
import java.util.function.IntUnaryOperator;

public class ArrayPopulate {

	public static void main(String[] args) {
		IntUnaryOperator fill = (int i) -> {
			return 13;
		};
		
		int[] myArray = new int[10];
		
		for (int i : myArray) {
			System.out.println("item = " + i);
		}
		
		Arrays.setAll(myArray, fill);
		
		for (int i : myArray) {
			System.out.println("item = " + i);
		}
	}
	
}
