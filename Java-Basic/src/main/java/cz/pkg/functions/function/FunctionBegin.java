package cz.pkg.functions.function;

import java.util.function.Function;

public class FunctionBegin {
  public static void main(String[] args) {
//    begin();
    passFnToMethod();
  }

  private static void passFnToMethod() {
    Function<String, Integer> str2int = (String text) -> Integer.valueOf(text);
    Function<Integer, String> int2str = (Integer number) -> String.valueOf(number);

    Integer i = transform("123", str2int);
    String s = transform(123, int2str);

    System.out.println(i);
    System.out.println(s);
  }

  private static <In, Out> Out transform(In in, Function<In, Out> function) {
    return function.apply(in);
  }

  private static void begin() {
    Function<String, Integer> str2int = (String text) -> {
      return Integer.valueOf(text);
    };

    String text = "123";
    System.out.println(str2int.apply(text));
  }
}
