package cz.pkg.functions.function;

import java.util.function.Function;

public class RestrictionTypes {
  public static void main(String[] args) {
    use();
  }

  public static <R extends Number> R parseString(Function<String, R> function, String value) {
    return function.apply(value);
  }

  private static void use() {
    String integer = "123";

//    System.out.println(parseString(str2int(integer), integer));
  }

//  private static Integer str2int(String value) {
//    return Integer.valueOf(value);
//  }

  private static Function<String, Integer> str2int = (String value) -> {
    return Integer.valueOf(value);
  };

}
