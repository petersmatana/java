package cz.pkg.functions.consumer;

import java.util.function.Consumer;

public class ConsumerBegin {

  private static int result = 0;

  public static void main(String[] args) {
    Consumer<Integer> printInteger = (Integer number) -> {
      System.out.println("int = " + number.toString());
    };

    Consumer<Integer> add = (Integer number) -> {
      result += number;
    };

    Consumer<Integer> substract = (Integer number) -> {
      result -= number;
    };

    add.andThen(add).andThen(add).accept(1);
    printInteger.accept(result);
  }
}
