package cz.pkg.functions.operator;

import java.util.function.BinaryOperator;

public class BinaryOperatorExample {
  public static void main(String[] args) {
    basic();
  }

  private static void basic() {
    BinaryOperator<Integer> add = (Integer number1, Integer number2) -> {
      return number1 + number2;
    };

    System.out.println(add.apply(1, 1));
  }
}
