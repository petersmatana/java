package cz.pkg.functions.operator;

import java.util.function.UnaryOperator;

public class UnaryFunction {
  public static void main(String[] args) {
    begin();
  }

  private static void begin() {
    UnaryOperator<Double> increment = (Double number) -> {
      return number + 1;
    };

    UnaryOperator<Double> decrement = (Double number) -> {
      return number - 1;
    };

    System.out.println(increment.apply(1.0));
  }
}
