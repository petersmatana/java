package cz.pkg.functions.operator;

import java.util.function.*;

public class SpecializedUnary {
  public static void main(String[] args) {
    upgradeBasic();
//    basic();
  }

  private static void upgradeBasic() {
    IntUnaryOperator intUnaryOperator;

    intUnaryOperator = (int number) -> {
      return number;
    };

    IntConsumer consumer = (int number) -> {
      System.out.println(number);
    };

    /*chci to vypsat ale takto se mi to nelibi.
    chci sout predat cislo ktery vypise, ne
    celou logiku
    * */
    System.out.println(businessLogic(intUnaryOperator, 5));
  }

  private static int businessLogic(IntUnaryOperator operator, int value) {
    return operator
        .andThen(x -> x + 2)
        .andThen(x -> x * 4)
        .applyAsInt(value);
  }

  private static void basic() {
    /*podobne jako Predicate ma Unary
    tyto specializovane operatory:
    * */
    IntUnaryOperator intUnaryOperator;
    DoubleUnaryOperator doubleUnaryOperator;
    LongUnaryOperator longUnaryOperator;

    intUnaryOperator = (int number) -> {
      return number;
    };

    IntConsumer consumer = (int number) -> {
      System.out.println(number);
    };

    /*chci to vypsat ale takto se mi to nelibi.
    chci sout predat cislo ktery vypise, ne
    celou logiku
    * */
    System.out.println(intUnaryOperator
        .andThen(x -> x + 2)
        .andThen(x -> x * 4)
        .applyAsInt(5));
  }
}
