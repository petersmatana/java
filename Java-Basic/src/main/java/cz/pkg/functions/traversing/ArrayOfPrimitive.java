package cz.pkg.functions.traversing;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ArrayOfPrimitive {

  public static void main(String[] args) {
//    basic();
    spliterator();
  }

  private static void spliterator() {
    List<Car> cars = Arrays.asList(
      new Car("vyrobce 1", "model 1"),
      new Car("vyrobce 1", "model 2"),
      new Car("vyrobce 2", "model 1")
    );

    Spliterator<Car> spliterator = cars.spliterator();
    spliterator.forEachRemaining((Car car) -> {
      System.out.println("auto = " + car.toString());
    });
  }

  private static void basic() {
    int[] arrayInts = {1, 2, 3};

    Iterator<Integer> integerIterator = new Iterator<Integer>() {
      private int index;

      @Override
      public boolean hasNext() {
        return arrayInts.length > index;
      }

      @Override
      public Integer next() {
        return arrayInts[index++];
      }
    };

    Consumer<Integer> consumer = (Integer number) -> {
      System.out.println("number = " + number);
    };

    integerIterator.forEachRemaining(consumer);
  }
}
