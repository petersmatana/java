package cz.pkg.functions.traversing;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Begin {
  public static void main(String[] args) {
//    begin();
    forEachRemaining();
  }

  private static void begin() {
    List<Car> cars = Arrays.asList(
      new Car("vyrobce 1", "model 1"),
      new Car("vyrobce 1", "model 2"),
      new Car("vyrobce 2", "model 1")
    );

    Iterator<Car> iterator = cars.iterator();
    while (iterator.hasNext()) {
      Car tmp = iterator.next();
      System.out.println(tmp);
    }
  }

  private static void forEachRemaining() {
    List<Car> cars = Arrays.asList(
        new Car("vyrobce 1", "model 1"),
        new Car("vyrobce 1", "model 2"),
        new Car("vyrobce 2", "model 1")
    );


  }
}
