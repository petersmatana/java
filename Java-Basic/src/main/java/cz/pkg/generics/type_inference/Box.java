package cz.pkg.generics.type_inference;

import java.util.List;

public abstract class Box<String> implements IBox<String> {
//  @Override
//  public String pick(String string, List<IBox> boxes) {
//    return null;
//  }

//  @Override
//  public <String> String pick(String string, List<IBox<String>> boxes) {
//    return null;
//  }

//  @Override
//  public <String> String pick(String string, List<IBox<java.lang.String>> boxes) {
//    return null;
//  }

  private String text;

  public Box(String text) {
    this.text = text;
  }

  @Override
  public String getText() {
    return text;
  }

  @Override
  public <String> String pick(String text, List<Box<String>> iBoxes) {
    for (IBox<String> l : iBoxes) {
      if (text.equals(l.getText())) {
        return l.getText();
      }
    }

    return null;
  }
}

