package cz.pkg.generics.type_inference;

import java.util.List;

public interface IBox<T> {
  T getText();

  /*Type Ingerence je to prvni <T> - prekladac
  si odvodi, jake typy jdou do funkce. v tomto
  pripade fce pracuje s typem T.
  fce pick vraci typ T a v argumentu prijima typ
  T text a List<Box<T>> boxes
  * */
  <T> T pick(T text, List<Box<T>> boxes);
}
