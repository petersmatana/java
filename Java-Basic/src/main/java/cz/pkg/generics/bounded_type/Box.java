package cz.pkg.generics.bounded_type;

public class Box<T extends Number> {
  private T t;

  public void set(T t) {
    this.t = t;
  }

  public T get() {
    return this.t;
  }
}
