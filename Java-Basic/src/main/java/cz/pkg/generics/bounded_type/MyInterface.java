package cz.pkg.generics.bounded_type;

import java.util.List;

public interface MyInterface<T extends Comparable> {
  // public <T> boolean find(List<T> list, T t);
  default <T> boolean find(List<T> list, T t) {

    return false;
  };
}
