package cz.pkg.generics.wildcards.upper_bounded;

import java.util.List;

public interface Fn {
  void process(List<? extends Number> list);
}
