package cz.pkg.generics.wildcards.unbounded_wildcard;

import java.util.List;

public class IntegerImpl implements Fn {
  @Override
  public void print(List<?> list) {
    for (Object item : list) {
      Integer tmp = (Integer) item;
      System.out.println(tmp.toString());
    }
  }
}
