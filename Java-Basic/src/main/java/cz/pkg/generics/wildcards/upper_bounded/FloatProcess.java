package cz.pkg.generics.wildcards.upper_bounded;

import java.util.List;

public class FloatProcess implements Fn {
  @Override
  public void process(List<? extends Number> list) {
    float result = 0;
    for (Number number : list) {
      result += number.floatValue();
    }

    System.out.println("Integer Process, float result = " + result);
  }
}
