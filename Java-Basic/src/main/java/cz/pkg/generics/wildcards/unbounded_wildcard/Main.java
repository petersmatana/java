package cz.pkg.generics.wildcards.unbounded_wildcard;

import java.util.Arrays;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    List<String> stringList = Arrays.asList("asd", "www", "lol");
    StringImpl string = new StringImpl();
    string.print(stringList);

    List<Integer> integerList = Arrays.asList(1, 2, 5);
    IntegerImpl integer = new IntegerImpl();
    integer.print(integerList);
  }
}
