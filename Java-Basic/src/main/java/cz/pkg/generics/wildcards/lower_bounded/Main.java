package cz.pkg.generics.wildcards.lower_bounded;

public class Main {

  /*Lower Bounded Wildcard funguje podobne jako
  Upper (popsanu u Main tridy). V tomto pripade
  specifikuji typ kteremu vyhovuji jeho predkove.
  <? extends Integer> vyhovuji Integer, Number a
  Object.
  * */
  public void main(String[] args) {

  }
}
