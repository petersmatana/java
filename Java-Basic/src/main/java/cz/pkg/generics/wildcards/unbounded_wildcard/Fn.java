package cz.pkg.generics.wildcards.unbounded_wildcard;

import java.util.List;

public interface Fn {
  public void print(List<?> list);
}
