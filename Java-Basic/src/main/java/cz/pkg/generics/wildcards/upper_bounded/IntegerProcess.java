package cz.pkg.generics.wildcards.upper_bounded;

import java.util.List;

public class IntegerProcess implements Fn {
  @Override
  public void process(List<? extends Number> list) {
    int result = 0;
    for (Number number : list) {
      result += number.intValue();
    }

    System.out.println("Integer Process, int result = " + result);
  }
}
