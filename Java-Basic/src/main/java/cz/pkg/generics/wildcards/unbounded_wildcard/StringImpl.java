package cz.pkg.generics.wildcards.unbounded_wildcard;

import java.util.List;

public class StringImpl implements Fn {
  @Override
  public void print(List<?> list) {
    for (Object item : list) {
      String tmp = (String) item;
      System.out.println(tmp);
    }
  }
}
