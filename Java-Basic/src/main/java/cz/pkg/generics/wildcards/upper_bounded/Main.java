package cz.pkg.generics.wildcards.upper_bounded;

import java.util.Arrays;
import java.util.List;

public class Main {

  /*Upper Bounded Wildcard se zapisuje jako <? extends Foo>
  coz potom vyhovuje pro Foo nebo toho kdo dedi z Foo.
  podobne jako <? implements Foo> pro interface.

  Upper Bounded Wildcard omezuje neznamy typ presne do daneho
  typu nebo jeho podtypu ~ extends.
  Podobne funguje Lower Bounded Wildcard.
  * */
  public static void main(String[] args) {
    List<Float> floatList = Arrays.asList(0.1f, 2.3f, 5.5f);

    FloatProcess fp = new FloatProcess();
    fp.process(floatList);

    List<Integer> integerList = Arrays.asList(1, 4, 2);

    IntegerProcess ip = new IntegerProcess();
    fp.process(integerList);
  }
}
