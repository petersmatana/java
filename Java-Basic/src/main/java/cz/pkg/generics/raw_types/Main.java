package cz.pkg.generics.raw_types;

public class Main {
  public static void main(String[] args) {
    // protoze jsem v deklaraci Box neuvedl genericky
    // operator, preklad mi vraci warning:
    // "uses unchecked or unsafe operations"
    Box rawBox = new Box();
    rawBox.set(10);

    System.out.println(rawBox.get());
  }
}
