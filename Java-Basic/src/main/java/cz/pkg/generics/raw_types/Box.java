package cz.pkg.generics.raw_types;

public class Box<T> {
  private T t;

  public void set(T t) {
    this.t = t;
  }

  public T get() {
    return this.t;
  }
}
