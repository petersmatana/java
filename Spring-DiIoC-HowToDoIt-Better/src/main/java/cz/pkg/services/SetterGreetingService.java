package cz.pkg.services;

import org.springframework.stereotype.Service;

/**
 * Setter Greeting Service.
 *
 * @author smonty
 * @version 1.0
 * @since 01-01-1970
 */
@Service
public class SetterGreetingService implements GreetingService {
  /**
   * Greeting method.
   *
   * @return Greeting message.
   */
  @Override
  public String sayGreeting() {
    return "Hello from Setter Greeting Service";
  }
}
