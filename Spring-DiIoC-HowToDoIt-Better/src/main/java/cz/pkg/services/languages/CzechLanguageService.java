package cz.pkg.services.languages;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("cz")
public class CzechLanguageService implements LanguageService {
  @Override
  public String sayHello() {
    return "Czech Language Service - Ahoj.";
  }
}
