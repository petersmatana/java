package cz.pkg.services.languages;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("en")
public class EnglishLanguageService implements LanguageService {
  @Override
  public String sayHello() {
    return "English Language Service - Hello.";
  }
}
