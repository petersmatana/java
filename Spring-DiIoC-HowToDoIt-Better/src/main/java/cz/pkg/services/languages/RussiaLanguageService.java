package cz.pkg.services.languages;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("ru")
public class RussiaLanguageService implements LanguageService {
  @Override
  public String sayHello() {
    return "Russia Language Service - Zdravstvuj.";
  }
}
