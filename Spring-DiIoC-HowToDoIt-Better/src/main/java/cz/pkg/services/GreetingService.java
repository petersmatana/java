package cz.pkg.services;

/**
 * Greeting Service interface.
 *
 * @author smonty
 * @version 1.0
 * @since 01-01-1970
 */
public interface GreetingService {
  /**
   * Greeting method.
   *
   * @return Greeting message.
   */
  String sayGreeting();
}
