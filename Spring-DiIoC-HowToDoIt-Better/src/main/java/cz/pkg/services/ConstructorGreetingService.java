package cz.pkg.services;

import org.springframework.stereotype.Service;

/**
 * Constructor Greeting Service.
 *
 * @author smonty
 * @version 1.0
 * @since 01-01-1970
 */
@Service
public class ConstructorGreetingService implements GreetingService {
  /**
   * Greeting method.
   *
   * @return Greeting service.
   */
  @Override
  public String sayGreeting() {
    return "Hello from Constructor Greeting Service";
  }
}
