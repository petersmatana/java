package cz.pkg;

import cz.pkg.controllers.ConstructorInjectedController;
import cz.pkg.controllers.SetterInjectedController;
import cz.pkg.controllers.languages.LanguageController;
import cz.pkg.services.PropertyGreetingService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Main class.
 *
 * @author smonty
 * @version 1.0
 * @since 01-01-1970
 */
@SpringBootApplication
public class Application {
  /**
   * Main method.
   * @param args Input params for application.
   */
  public static void main(String[] args) {
    ApplicationContext context = SpringApplication.run(Application.class, args);

    System.out.println(context.getBean(ConstructorInjectedController.class).sayHello());
    System.out.println(context.getBean(SetterInjectedController.class).sayHello());
    System.out.println(context.getBean(PropertyGreetingService.class).sayGreeting());

    context.getBean(LanguageController.class).sayHello();
  }
}
