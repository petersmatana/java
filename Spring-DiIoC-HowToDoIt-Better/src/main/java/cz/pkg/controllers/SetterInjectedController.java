package cz.pkg.controllers;

import cz.pkg.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * Javadoc.
 */
@Controller
public class SetterInjectedController {
  /**
   * Greeting Service.
   */
  private GreetingService greetingService;

  @Autowired
  @Qualifier("setterGreetingService")
  public void setGreetingService(GreetingService greetingService) {
    this.greetingService = greetingService;
  }

  public String sayHello() {
    return this.greetingService.sayGreeting();
  }
}
