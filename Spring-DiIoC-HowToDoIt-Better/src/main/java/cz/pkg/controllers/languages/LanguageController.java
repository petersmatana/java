package cz.pkg.controllers.languages;

import cz.pkg.services.languages.LanguageService;
import org.springframework.stereotype.Controller;

@Controller
public class LanguageController {
  private LanguageService languageService;

  public LanguageController(LanguageService service) {
    this.languageService = service;
  }

  public void sayHello() {
    System.out.println(this.languageService.sayHello());
  }
}
