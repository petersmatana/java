package cz.pkg.controllers;

import cz.pkg.services.GreetingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * Constructor Injected Controller.
 *
 * @author smonty
 * @version 1.0
 * @since 01-01-1970
 */
@Controller
public class ConstructorInjectedController {
  /**
   * Greeting Service.
   */
  private GreetingService greetingService;

  /**
   * Class constructor.
   *
   * @param greetingService Greeting Service.
   */
  public ConstructorInjectedController(final @Qualifier("constructorGreetingService") GreetingService greetingService) {
    this.greetingService = greetingService;
  }

  /**
   * Say Hello.
   *
   * @return Greeting message.
   */
  public String sayHello() {
    return this.greetingService.sayGreeting();
  }
}
